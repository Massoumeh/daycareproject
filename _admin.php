<?php
require_once '_setup.php';

// ADMIN INTERFACE EXAMPLE CRUD OPERATIONS HANDLING

$app->get('/admin', function ($request, $response, $args) {
    $usersList = DB::query("SELECT * FROM users");
    return $this->view->render($response, 'admin/index.html.twig', ['usersList' => $usersList]);
});

$app->get('/admin/users/list', function ($request, $response, $args) {
    $usersList = DB::query("SELECT id,role,firstName,lastName,email,password FROM users");
    return $this->view->render($response, 'admin/users_list.html.twig', ['usersList' => $usersList]);
});


// STATE 1: first display
$app->get('/admin/users/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($args['op'] == 'add' && !empty($args['id'])) || ($args['op'] == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($args['op'] == 'edit') {
        $user = DB::queryFirstRow("SELECT id,role,firstName,lastName,email,password FROM users WHERE id=%d", $args['id']);
        if (!$user) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $user = [];
    }
    return $this->view->render($response, 'admin/users_addedit.html.twig', ['v' => $user, 'op' => $args['op']]);
});

// STATE 2&3: receiving submission
$app->post('/admin/users/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
    $op = $args['op'];
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($op == 'add' && !empty($args['id'])) || ($op == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    $firstName = $request->getParam('firstName');
    $lastName = $request->getParam('lastName');
    $role = $request->getParam('role');
    $email = $request->getParam('email');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');
    //
    $errorList = array();
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $firstName) != 1) { // no match
        array_push($errorList, "First name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
        $firstName = "";
    }
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $lastName) != 1) { // no match
        array_push($errorList, "Last name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
        $lastName = "";
    }
    

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
        $email = "";
    } else {
        // is email already in use BY ANOTHER ACCOUNT???
        if ($op == 'edit') {
            $record = DB::queryFirstRow("SELECT id,role,firstName,lastName,email,password FROM users WHERE email=%s AND id != %d", $email, $args['id'] );
        } else { // add has no id yet
            $record = DB::queryFirstRow("SELECT id,role,firstName,lastName,email,password FROM users WHERE email=%s", $email);
        }
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }
  
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorList, "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }
    //
    if ($errorList) {
        return $this->view->render($response, 'admin/users_addedit.html.twig',
                [ 'errorList' => $errorList, 'v' => ['firstName' => $firstName, 'lastName' => $lastName, 'email' => $email ]  ]);
    } else {
        if ($op == 'add') {
            DB::insert('users', ['firstName' => $firstName, 'lastName' => $lastName, 'email' => $email, 'password' => $pass1, 'role' => $role]);
            return $this->view->render($response, 'admin/users_addedit_success.html.twig', ['op' => $op ]);
        } else {
            $data = ['firstName' => $firstName, 'lastName' => $lastName, 'email' => $email, 'password' => $pass1, 'role' => $role];
            if ($pass1 != '') { // only update the password if it was provided
                $data['password'] = $pass1;
            }
            DB::update('users', $data, "id=%d", $args['id']);
            return $this->view->render($response, 'admin/users_addedit_success.html.twig', ['op' => $op ]);
        }
    }
});


// STATE 1: first display
$app->get('/admin/users/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $user = DB::queryFirstRow("SELECT id,role,firstName,lastName,email,password FROM users WHERE id=%d", $args['id']);
    if (!$user) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/users_delete.html.twig', ['v' => $user] );
});

// STATE 1: first display
$app->post('/admin/users/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('users', "id=%d", $args['id']);
    return $this->view->render($response, 'admin/users_delete_success.html.twig' );
});

// Attach middleware that verifies only Admin can access /admin... URLs

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

// Function to check string starting 
// with given substring 
function startsWith($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 

$app->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
    $url = $request->getUri()->getPath();
    if (startsWith($url, "/admin")) {
        if (!isset($_SESSION['user']) || $_SESSION['user']['role'] == 0) { // refuse if user not logged in AS ADMIN
            $response = $response->withStatus(403);
            return $this->view->render($response, 'admin/error_access_denied.html.twig');
        }
    }
    return $next($request, $response);
});



