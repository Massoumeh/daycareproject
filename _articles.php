<?php
require_once '_setup.php';

use Respect\Validation\Validator as Validator;
use Slim\Http\UploadedFile;

// TODO: Define app routes
// Define app routes
$app->get('/articles', function ($request, $response, $args) {
    $articleList = DB::query("SELECT a.id, a.authorId, a.creationTS, a.title, a.body, u.firstName, u.lastName "
        . "FROM articles as a, users as u WHERE a.authorId = u.id ORDER BY a.id DESC");
    foreach ($articleList as &$article) {
        // format posted date
        $datetime = strtotime($article['creationTS']);
        $postedDate = date('M d, Y \a\t H:i:s', $datetime );
        $article['postedDate'] = $postedDate;
        // only show the beginning of body if it's long, also remove html tags
        $fullBodyNoTags = strip_tags($article['body']);
        $bodyPreview = substr(strip_tags($fullBodyNoTags), 0, 100); // FIXME
        $bodyPreview .= (strlen($fullBodyNoTags) > strlen($bodyPreview)) ? "..." : "";
        $article['body'] = $bodyPreview;
    }
    return $this->view->render($response, 'addarticle.html.twig', ['list' => $articleList]);
    //print_r($articleList);
    //return $response->write("");
});



$articlesPerPage = 3;

// PAGINATION WITHOUT AJAX - JUST A-HREFs
$app->get('/paginated[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    global $articlesPerPage;
    $pageNo = $args['pageNo'] ?? 1;
    $articlesCount = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM articles");
    $articleList = DB::query("SELECT a.id, a.authorId, a.creationTS, a.title, a.body, u.firstName, u.lastName "
        . "FROM articles as a, users as u WHERE a.authorId = u.id ORDER BY a.id DESC LIMIT %d OFFSET %d",
             $articlesPerPage, ($pageNo - 1) * $articlesPerPage);
    foreach ($articleList as &$article) {
        // format posted date
        $datetime = strtotime($article['creationTS']);
        $postedDate = date('M d, Y \a\t H:i:s', $datetime );
        $article['postedDate'] = $postedDate;
        // only show the beginning of body if it's long, also remove html tags
        $fullBodyNoTags = strip_tags($article['body']);
        $bodyPreview = substr(strip_tags($fullBodyNoTags), 0, 100); // FIXME
        $bodyPreview .= (strlen($fullBodyNoTags) > strlen($bodyPreview)) ? "..." : "";
        $article['body'] = $bodyPreview;
    }
    $maxPages = ceil($articlesCount / $articlesPerPage);
    $prevNo = ($pageNo > 1) ? $pageNo-1 : '';
    $nextNo = ($pageNo < $maxPages) ? $pageNo+1 : '';
    return $this->view->render($response, 'paginated.html.twig', [
            'list' => $articleList,
            'maxPages' => $maxPages,
            'pageNo' => $pageNo,
            'prevNo' => $prevNo,
            'nextNo' => $nextNo
        ]);
});

// PAGINATION WITH AJAX
$app->get('/ajaxpaginated[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    global $articlesPerPage;
    $pageNo = $args['pageNo'] ?? 1;
    $articlesCount = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM articles");
    $maxPages = ceil($articlesCount / $articlesPerPage);
    return $this->view->render($response, 'ajaxpaginated.html.twig', [
            'maxPages' => $maxPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/ajaxsinglepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    global $articlesPerPage;
    $pageNo = $args['pageNo'] ?? 1;
    $articleList = DB::query("SELECT a.id, a.authorId, a.creationTS, a.title, a.body,u.firstName, u.lastName "
        . "FROM articles as a, users as u WHERE a.authorId = u.id ORDER BY a.id DESC LIMIT %d OFFSET %d",
             $articlesPerPage, ($pageNo - 1) * $articlesPerPage);
    foreach ($articleList as &$article) {
        // format posted date
        $datetime = strtotime($article['creationTS']);
        $postedDate = date('M d, Y \a\t H:i:s', $datetime );
        $article['postedDate'] = $postedDate;
        // only show the beginning of body if it's long, also remove html tags
        $fullBodyNoTags = strip_tags($article['body']);
        $bodyPreview = substr(strip_tags($fullBodyNoTags), 0, 100); // FIXME
        $bodyPreview .= (strlen($fullBodyNoTags) > strlen($bodyPreview)) ? "..." : "";
        $article['body'] = $bodyPreview;
    }
    return $this->view->render($response, 'ajaxsinglepage.html.twig', [
            'list' => $articleList
        ]);
});

$app->map(['GET', 'POST'],'/article/{id:[0-9]+}', function ($request, $response, $args) {
    $articleId = $args['id'];
    // step 1: fetch article and author info
    $article = DB::queryFirstRow("SELECT a.id, a.authorId, a.creationTS, a.title, a.body, a.imagePath, u.firstName, u.lastName "
            . "FROM articles as a, users as u WHERE a.authorId = u.id AND a.id = %d", $articleId);
    if (!$article) { // TODO: use Slim's default 404 page instead of our custom one
        $response = $response->withStatus(404);
        return $this->view->render($response, 'article_not_found.html.twig');
    }
    $datetime = strtotime($article['creationTS']);
    $postedDate = date('M d, Y \a\t H:i:s', $datetime );
    $article['postedDate'] = $postedDate;
    // step 2: handle comment submission if there is one
    if ($request->getMethod() == "POST" ) {
        // is user authenticated?
        if (!isset($_SESSION['user'])) { // refuse if user not logged in
            $response = $response->withStatus(403);
            return $this->view->render($response, 'error_access_denied.html.twig');
        }
        $authorId = $_SESSION['user']['id'];
        $body = $request->getParam('body');
        // TODO: we could check other things, like banned words
        if (strlen($body) > 0) {
            DB::insert('comments', [
                'actId' => $articleId,
                'userId' => $authorId,
                'body' => $body
            ]);
        }
    }
    // step 3: fetch article comments
    $commentsList = DB::query("SELECT c.id, u.firstName, u.lastName  as userId, c.creationTS, c.body FROM comments c, users u "
                . "WHERE c.userId=u.id AND c.actId = %d ORDER BY c.id", $articleId);
    foreach ($commentsList as &$comment) {
        $datetime = strtotime($comment['creationTS']);
        $postedDate = date('M d, Y \a\t H:i:s', $datetime );
        $comment['postedDate'] = $postedDate;
    }
    //
    return $this->view->render($response, 'article.html.twig', ['a' => $article, 'commentsList' => $commentsList]);
});

// STATE 1: first display
$app->get('/addarticle', function ($request, $response, $args) {
    if (!isset($_SESSION['user'])) { // refuse if user not logged in
        $response = $response->withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }
    return $this->view->render($response, 'addarticle.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/addarticle', function ($request, $response, $args) {
    if (!isset($_SESSION['user'])) { // refuse if user not logged in
        $response = $response->withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }
    $title = $request->getParam('title');
    $body = $request->getParam('body');
    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
    // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection
    $body = strip_tags($body, "<p><ul><li><em><strong><i><b><ol><h3><h4><h5><span>");
    //
    $errorList = array();
    // EXAMPLE of Validator use from respect/validation package
    if (!Validator::stringType()->length(2, 100)->alnum(' .:,/')->validate($title)) {
    // if (strlen($title) < 2 || strlen($title) > 100) {
        array_push($errorList, "Title must be 2-100 characters long, alphanumeric characters only");
        // keep the title even if invalid
    }
    if (!Validator::stringType()->length(2, 10000)->validate($body)) {
    // if (strlen($body) < 2 || strlen($body) > 10000) {
        array_push($errorList, "Body must be 2-10000 characters long");
        // keep the body even if invalid
    }
    // verify image
    $hasPhoto = false;
    $uploadedImage = $request->getUploadedFiles()['image'];
    if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto = true;
        $result = verifyUploadedPhoto($uploadedImage);
        if ($result !== TRUE) {
            $errorList[] = $result;
        } 
    }
    //
    if ($errorList) {
        return $this->view->render($response, 'addarticle.html.twig',
                [ 'errorList' => $errorList, 'v' => ['title' => $title, 'body' => $body ]  ]);
    } else {
        if ($hasPhoto) {
            $directory = $this->get('upload_directory');
            $uploadedImagePath = moveUploadedFile($directory, $uploadedImage);
        }
        $authorId = $_SESSION['user']['id'];
        DB::insert('articles', ['userId' => $authorId, 'title' => $title, 'body' => $body, 'imagePath' => $uploadedImagePath]);
        $articleId = DB::insertId();
        return $this->view->render($response, 'addarticle_success.html.twig', ['id' => $articleId]);
    }
});

function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

// returns TRUE on success
// returns a string with error message on failure
function verifyUploadedPhoto($photo, &$mime = null) {
        if ($photo->getError() != 0) {
            return "Error uploading photo " . $photo->getError();
        } 
        if ($photo->getSize() > 1024*1024) { // 1MB
            return "File too big. 1MB max is allowed.";
        }
        $info = getimagesize($photo->file);
        if (!$info) {
            return "File is not an image";
        }
        // echo "\n\nimage info\n";
        // print_r($info);
        if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
            return "Width and height must be within 200-1000 pixels range";
        }
        $ext = "";
        switch ($info['mime']) {
            case 'image/jpeg': $ext = "jpg"; break;
            case 'image/gif': $ext = "gif"; break;
            case 'image/png': $ext = "png"; break;
            default:
                return "Only JPG, GIF and PNG file types are allowed";
        } 
        if (!is_null($mime)) {
            $mime = $info['mime'];
        }
        return TRUE;
}

