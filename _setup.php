<?php
session_start(); // enable Sessions mechanism for the entire web application

require_once 'vendor/autoload.php';


use Respect\Validation\Validator as Validator;



use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd21.com") !== false) {
    //hosting on ipd21
    DB::$dbName = 'cp4976_daycareproject';
    DB::$user = 'cp4976_daycareproject';
    DB::$password = 'ch9wLb0kzzyvHOkkDq';
} else {
    DB::$dbName = 'daycareprojects';
    DB::$user = 'daycareprojects';
    DB::$password = 'YjJ0aNPMoP4y47NM';  // 'ch9wLb0kzzyvHOkkDq'
    DB::$port = 3333;   
}
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    http_response_code(500);
    header('Content-Type: application/json');
    echo json_encode("500 - Database error");
    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);
    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
    die();
}

use Slim\Http\UploadedFile;

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// File upload directory
$container['upload_directory'] = __DIR__ . '/uploads';

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/cache',
        'debug' => true, // This line should enable debug mode
    ]);
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['user'] ?? null );



$passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';


