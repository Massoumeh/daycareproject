<?php
require_once '_setup.php';


$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());

// STATE 1: first display
$app->get('/register', function ($request, $response, $args) {
    return $this->view->render($response,  'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function ($request, $response, $args) {
    $firstName = $request->getParam('firstName');
    $lastName = $request->getParam('lastName');
    $gender = $request->getParam('gender') ?? 'male';
    $phoneNo = $request->getParam('phoneNo');
    $address = $request->getParam('address');
    $city = $request->getParam('city');
    $postalCode = $request->getParam('postalCode');
    $role = $request->getParam('role') ?? 'parent';
    $email = $request->getParam('email');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');
    //


    $errorList = array();
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $firstName) != 1) { // no match
        array_push($errorList, "First name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
        $firstName = "";
    }
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $lastName) != 1) { // no match
        array_push($errorList, "Last name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
        $lastName = "";
    }

    if (preg_match('/^[0-9]{3}[0-9]{3}[0-9]{4}$/', $phoneNo) != 1) { // no match
        array_push($errorList, "phoneNo must be 12 digits long");
        $phoneNo = "";
    }
    if (preg_match('/^[a-zA-Z0-9\ \\._\#\'"-]{4,320}$/', $address) != 1) { // no match
        array_push($errorList, "Address must be 4-320 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, pound sign or minus sign.");
        $address = "";
    }
    if (preg_match('/^[a-zA-Z\ \\._\'"-]{2,250}$/', $city) != 1) { // no match
        array_push($errorList, "City must be 2-250 characters long.");
        $city = "";
    }
    if (preg_match('/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/', $postalCode) != 1) { // no match
        array_push($errorList, "Input Valid Postal Code");
        $postalCode = "";
    }


    // verify email
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
        $email = "";
    } else {
        // is email already in use?
        $record = DB::queryFirstRow("SELECT id FROM users WHERE email=%s", $email);
        if ($record) {
            array_push($errorList, "This email is already registered!");
            $email = "";
        }
    }
    //
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorList, "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }

    
    //
    if ($errorList) {
        return $this->view->render($response, 'register.html.twig',
                [ 'errorList' => $errorList, 'v' => ['firstName' => $firstName, 'lastName' => $lastName, 'phoneNo' => $phoneNo, 
                'address' => $address,'city' => $city, 'postalCode' => $postalCode, 'email' => $email]  ]);
    } else {
        
        //
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
        $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT); // PASSWORD_ARGON2ID);
        DB::insert('users', ['firstName' => $firstName, 'lastName' => $lastName, 'phoneNo' => $phoneNo, 'address' => $address,'city' => $city, 'postalCode' => $postalCode, 'email' => $email, 'password' => $pwdHashed,
        'gender' => $gender, 'role' => $role]);
        return $this->view->render($response, 'register_success.html.twig');
    }
});

$app->get('/teacher', function ($request, $response, $args) {
    return $this->view->render($response, 'teacher.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/teacher', function ($request, $response, $args) use ($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    //
    $record = DB::queryFirstRow("SELECT id,role,firstName,lastName,email,password FROM users WHERE email=%s AND role='teacher'", $email);
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }
        // WARNING: only temporary solution to allow for old plain-text passwords to continue to work
        // Plain text passwords comparison
        else if ($record['password'] == $password) {
            $loginSuccess = true;
        }
    }
  //
    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for email %s from %s", $email, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'teacher.html.twig', [ 'error' => true ]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['id'], $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'teacher_login_success.html.twig', ['userSession' => $_SESSION['user'] ] );
    }
});

$app->get('/teacher/profile', function ($request, $response, $args) {
    $user = $_SESSION['user'];
    $result = DB::query("SELECT users.firstName,users.lastName,users.gender,users.email,users.phoneNo,teachers.id,teachers.status,teachers.class,teachers.imagePath
        FROM teachers,users where users.id=teachers.id and teachers.userId=%d", $user['id']);
        $_SESSION['user'] = $result;
         return $this->view->render($response, 'teacher_profile.html.twig',  ['userSession' => $_SESSION['user']]  );
 
});

// STATE 1: first display
$app->get('/parent', function ($request, $response, $args) {
    return $this->view->render($response, 'parent.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/parent', function ($request, $response, $args) use ($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    //
    $record = DB::queryFirstRow("SELECT id,role,firstName,lastName,email,password FROM users WHERE email=%s AND role='parent'", $email);
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }
        // WARNING: only temporary solution to allow for old plain-text passwords to continue to work
        // Plain text passwords comparison
        else if ($record['password'] == $password) {
            $loginSuccess = true;
        }
    }
  //
    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for email %s from %s", $email, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'parent.html.twig', [ 'error' => true ]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['id'], $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'parent_login_success.html.twig', ['userSession' => $_SESSION['user'] ] );
    }
});

$app->get('/parent/profile', function ($request, $response, $args) {
    $user = $_SESSION['user'];
    $result = DB::query("SELECT users.firstName,users.lastName,users.gender,users.email,users.phoneNo,parents.id,parents.workPhone,parents.workAddress
        FROM parents,users where users.id=parents.id and parents.userid=%d", $user['id'] );
        $_SESSION['user'] = $result;
         return $this->view->render($response, 'parent_profile.html.twig',  ['userSession' => $_SESSION['user']]  );
 
});

$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    unset($_SESSION['user']);
    return $this->view->render($response, 'logout.html.twig', ['userSession' => null ]);
});


// LOGIN / LOGOUT USING FLASH MESSAGES TO CONFIRM THE ACTION

function setFlashMessage($message) {
    $_SESSION['flashMessage'] = $message;
}

// returns empty string if no message, otherwise returns string with message and clears is
function getAndClearFlashMessage() {
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}

$app->get('/activity', function ($request, $response, $args) {
    return $this->view->render($response, 'activity.html.twig');
});


$app->get('/team', function ($request, $response, $args) {
    return $this->view->render($response, 'team.html.twig');
});

$app->get('/message', function ($request, $response, $args) {
    return $this->view->render($response, 'message.html.twig');
});

$app->get('/event', function ($request, $response, $args) {
    return $this->view->render($response, 'event.html.twig');
});
